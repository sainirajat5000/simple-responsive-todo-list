const createNewTask = (inp) => {
  let newTask = document.createElement("div");
  let taskFiled = document.createElement("span");
  let data = document.createTextNode(inp);
  let deleteButtonForTask = document.createElement("button");
  deleteButtonForTask.setAttribute("class", "deleteButton");
  deleteButtonForTask.textContent = "Delete";

  let inProgressButton = document.createElement("button");
  inProgressButton.setAttribute("class", "inprogress");
  inProgressButton.textContent = "Inprogress";
  taskFiled.appendChild(data);
  newTask.appendChild(taskFiled);
  newTask.appendChild(deleteButtonForTask);
  newTask.appendChild(inProgressButton);
  newTask.setAttribute("class", "delete");
  return newTask;
};

const newToDoAdded = () => {
  const inputToDo = document.getElementById("newToINput");
  const toDoList = document.getElementById("toDoList");
  if (inputToDo.value != "") {
    const newTaskCreated = createNewTask(inputToDo.value);
    toDoList.appendChild(newTaskCreated);
    inputToDo.value = "";
  }
};
const deleteTask = (nodeToRemove) => {
  const parentNode = nodeToRemove.parentNode;
  parentNode.removeChild(nodeToRemove);
};
const moveToProgress = (parentDiv, deleteButton) => {
    parentDiv.removeChild(deleteButton);
    const doneButton = document.createElement('button');
    doneButton.setAttribute('class','doneButton');
    doneButton.textContent = "Done";
    parentDiv.appendChild(doneButton);
    return parentDiv;
}
const moveToDone = (parentDiv,e) => {
    parentDiv.removeChild(e.target);
    return parentDiv;
}

document.querySelector("#allLists").addEventListener("click", (e) => {
  const clickedButton = e.target.innerHTML;
  const parentDiv = e.target.parentNode;
  if (clickedButton == "Delete") {
    deleteTask(parentDiv);
  } else if(clickedButton == "Inprogress" ) {
      const progressList = document.getElementById('inProgressList');
      const newInProgressTask = moveToProgress(parentDiv,e.target);
      progressList.appendChild(newInProgressTask);
  } else if (clickedButton == "Done") {
      const doneList = document.getElementById('doneList');
      const newInDoneTask = moveToDone(parentDiv,e);
      doneList.appendChild(newInDoneTask);
  }
});
